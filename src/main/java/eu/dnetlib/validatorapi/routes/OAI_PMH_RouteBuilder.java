package eu.dnetlib.validatorapi.routes;

import eu.dnetlib.validator2.validation.guideline.openaire.LiteratureGuidelinesV4Profile;
import eu.dnetlib.validatorapi.entities.ValidationIssue;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import eu.dnetlib.validatorapi.entities.ValidationRuleResult;
import eu.dnetlib.validatorapi.processors.XmlProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Component
public class OAI_PMH_RouteBuilder extends RouteBuilder {
/*
    private final String oaiEndpoint = "oaipmh://http://repositorium.sdum.uminho.pt/oai/request";
    private AbstractOpenAireProfile profile;
    private long maxNumberOfRecords = 0;
    private ValidationJob validationJob;

    private final ValidationJobRepository validationJobRepository;
    private final ValidationIssueRepository validationIssueRepository;
    private final ValidationResultRepository validationResultRepository;

    public OAI_PMH_RouteBuilder(String oaiEndpoint, AbstractOpenAireProfile profile, ValidationJob validationJob,
                       long maxNumberOfRecords, final ValidationJobRepository validationJobRepository,
                       final ValidationIssueRepository validationIssueRepository,
                       final ValidationResultRepository validationResultRepository) {
        this.validationJob = validationJob;
        this.profile = profile;
        this.maxNumberOfRecords = maxNumberOfRecords;
        this.validationJobRepository = validationJobRepository;
        this.validationIssueRepository = validationIssueRepository;
        this.validationResultRepository = validationResultRepository;
    }*/

    @Override
    public void configure() throws Exception {

       /* String id1 = new Date().getTime()+"";
        String id2 = new Date().getTime()+1+"";
        String date = new Date().toString();
*/
        from("direct:startProcessing")
                .toD("${body}") // Use dynamic endpoint from the message body (the endpoint set by OaiPmhRoute)
                .routeId("Process_OAI_Response")
                .split(xpath("//*[local-name()='record']"))
                .process(new XmlProcessor(new LiteratureGuidelinesV4Profile(), new ValidationJob(), 1))
                    .choice()
                        .when(simple("${body[results]} && ${header.MyHeader} != 'stop'"))
                            .split(simple("${body[results]}"))
                            .to("jpa:" + ValidationRuleResult.class.getName() + "?usePersist=true")
                        .stop() // Stop the route when this condition is true
                    .endChoice()
                .end()
                    .choice()
                        .when(simple("${body[issues]} && ${header.MyHeader} != 'stop'"))
                            .split(simple("${body[issues]}"))
                            .to("jpa:" + ValidationIssue.class.getName() + "?usePersist=true")
                        .stop() // Stop the route when this condition is true
                    .endChoice()
                .end()
                .choice()
                    .when(header("MyHeader").isEqualTo("stop"))
                        .to("direct:saveToDatabase")
                        .to("controlbus:route?routeId=Process_OAI_Response&action=stop")
                    .endChoice()
                .end();




        /*from("direct:getRecords")
                .toD("${header.oaiEndpoint}")
                .routeId(date)
                .split(xpath("//*[local-name()='record']"))
                .log("${body}")
                .process(new XmlProcessor(profile, validationJob, maxNumberOfRecords))
                .choice()
                .when(simple("${body[issues]} && ${header.MyHeader} != 'stop'"))
                .log("HERE!!!")
                .split(simple("${body[issues]}"))
                .to("jpa:"+ ValidationRuleResult.class.getName()+ "?usePersist=true")
                .endChoice()
                .end()
                .choice()
                .when(simple("${body[results]} && ${header.MyHeader} != 'stop'"))
                .log("THERE")
                .split(simple("${body[results]}"))
                .to("jpa:" + ValidationIssue.class.getName() + "?usePersist=true")
                .endChoice()
                .end()
                .choice()
                .when(header("MyHeader").isEqualTo("stop"))
                .to("direct:saveToDatabase")
                .to("controlbus:route?routeId="+date+"&action=stop")
                .endChoice()
                .end();


        from("direct:saveToDatabase")
                .process(new DataBaseProcessor())
                .to("jpa:" + ValidationJob.class.getName() + "?useExecuteUpdate=true");*/

        /*
        from(oaiEndpoint)
                .multicast().parallelProcessing()
                    .to("direct:guidelinesProcessor")
                    .to("direct:fairProcessor")
                .end();

        from("direct:guidelinesProcessor")
                .routeId(id1)
                .split(xpath("//*[local-name()='record']"))
          //TODO      .process(new XmlProcessor(profile, validationJob, maxNumberOfRecords))
                //.log("PROCESSOR 1111 \n\n\n ${body}")
                .choice().when(header("MyHeader").isEqualTo("stop"))
                //.to("direct:saveToDatabase")
                .to("controlbus:route?routeId="+id1+"&action=stop")
                .endChoice();

        from("direct:fairProcessor")
                .routeId(id2)
                .split(xpath("//*[local-name()='record']"))
                //.log("PROCESSOR 222 \n\n\n ${body}")
         //TODO       .process(new XmlProcessor(new FAIR_Data_GuidelinesProfile(), validationJob, maxNumberOfRecords))
                .choice().when(header("MyHeader").isEqualTo("stop"))
                //.to("direct:saveToDatabase")
                .to("controlbus:route?routeId="+id2+"&action=stop")
                .endChoice();


        from("direct:saveToDatabase") //TODO FIX THIS with JPA
                .process(new DataBaseProcessor());


         */
    }

    private List<String> extractSets(String xml) {
        List<String> setXmls = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(new StringReader(xml));
            Document document = builder.parse(inputSource);

            NodeList setElements = document.getElementsByTagName("set");
            for (int i = 0; i < setElements.getLength(); i++) {
                Node setElement = setElements.item(i);
                String setXml = nodeToString(setElement);
                setXmls.add(setXml);
            }
        } catch (Exception e) {
            // Handle any exceptions that occur during XML parsing or processing
            log.error(e.getMessage());
        }

        return setXmls;
    }

    private String nodeToString(Node node) throws TransformerException {
        StringWriter sw = new StringWriter();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(node), new StreamResult(sw));
        return sw.toString();
    }
}
