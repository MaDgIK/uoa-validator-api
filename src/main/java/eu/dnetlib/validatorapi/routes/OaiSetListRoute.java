package eu.dnetlib.validatorapi.routes;

import eu.dnetlib.validatorapi.processors.SetsProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.TypeConversionException;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.engine.DefaultShutdownStrategy;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

@Component
public class OaiSetListRoute extends RouteBuilder {


    @Override
    public void configure() throws Exception {

        // Access the DefaultShutdownStrategy
        DefaultShutdownStrategy shutdownStrategy = (DefaultShutdownStrategy) getContext().getShutdownStrategy();

        // Set the shutdown timeout in milliseconds (e.g., 10 seconds)
        shutdownStrategy.setTimeout(1);

        onException(TypeConversionException.class)
                .maximumRedeliveries(0)
                .handled(true)
                .end();

        onException(ClientProtocolException.class)
                .maximumRedeliveries(0)
                .handled(true)
                .end();

        onException(UnknownHostException.class)
                .maximumRedeliveries(0)
                .handled(true)
                .end();

        onException(ClientProtocolException.class)
                .maximumRedeliveries(0)
                .handled(true)
                .end();

        onException(TimeoutException.class)
                .handled(true)
                .process(exchange -> {
                    exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_REQUEST_TIMEOUT);
                    exchange.getMessage().setHeader(Exchange.CONTENT_TYPE, "application/json");
                    exchange.getIn().setBody("{\"error_code\":\"408\",\"message\":\"Unable to retrieve sets. Server response takes too long.\"}");
                });

        from("direct:getResponse")
                .routeId("myRoute")
                .process(exchange -> {
                    String endpoint = exchange.getIn().getHeader("endpoint", String.class);
                    exchange.getIn().setHeader("dynamicEndpoint", endpoint);
                }).recipientList(header("dynamicEndpoint"))
                .parallelProcessing()
                .timeout(20000)
                .process(new SetsProcessor())
                .end();
    }
}
