package eu.dnetlib.validatorapi.entities;

public enum Progress {
    STOPPED,
    COMPLETED,
    IN_PROGRESS
}
