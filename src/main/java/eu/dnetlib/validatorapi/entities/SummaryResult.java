package eu.dnetlib.validatorapi.entities;

import eu.dnetlib.validatorapi.controllers.SummaryResultId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="summary_result")
@IdClass(SummaryResultId.class)
public class SummaryResult implements Serializable {
    @Id
    @Column(name="rule_name")
    public String ruleName;

    @Id
    @Column(name="validation_job_id")
    public int validationJobId;

    @Column(name="rule_weight")
    public int rule_weight;

    @Column(name="requirement_level")
    public String requirement_level;

    @Column(name="description")
    public String description;

    @Column(name="fair_principles")
    public String fair_principles;

    @Column(name="link")
    public String link;

    @Column(name="guidelines")
    public String guidelines;

    @Column(name = "internal_error")
    public String internal_error;

    @Column(name="rule_status")
    public String rule_status;

    @Column(name = "passed_records")
    public long passed_records;

    @Column(name="failed_records")
    public long failed_records;

    @Column(name="has_errors")
    public boolean has_errors;

    @Column(name="has_warnings")
    public boolean has_warnings;

    public SummaryResult(){}

    public SummaryResult(int validationJobId, String rule_name) {
        this.validationJobId = validationJobId;
        this.ruleName = rule_name;
    }
    public SummaryResult(String rule_name, long passed_records, long failed_records) {
        this.ruleName = rule_name;
        this.passed_records = passed_records;
        this.failed_records = failed_records;
    }

    public SummaryResult(String rule_name, int rule_weight, long passed_records, long failed_records) {
        this.ruleName = rule_name;
        this.rule_weight = rule_weight;
        this.passed_records = passed_records;
        this.failed_records = failed_records;
    }


    public SummaryResult(String rule_name, int rule_weight, String requirement_level, String description, String fair_principles,
                         String link, String guidelines, String internal_error, long passed_records, long failed_records,
                         String rule_status, boolean has_errors, boolean has_warnings) {
        this.ruleName = rule_name;
        this.rule_weight = rule_weight;
        this.requirement_level = requirement_level;
        this.description = description;
        this.fair_principles = fair_principles;
        this.link = link;
        this.guidelines = guidelines;
        this.internal_error = internal_error;
        this.rule_status = rule_status;
        this.passed_records = passed_records;
        this.failed_records = failed_records;
        this.has_errors = has_errors;
        this.has_warnings = has_warnings;
    }



    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public int getValidationJobId() {
        return validationJobId;
    }

    public void setValidationJobId(int validationJobId) {
        this.validationJobId = validationJobId;
    }

    public int getRule_weight() {
        return rule_weight;
    }

    public void setRule_weight(int rule_weight) {
        this.rule_weight = rule_weight;
    }

    public String getRequirement_level() {
        return requirement_level;
    }

    public void setRequirement_level(String requirement_level) {
        this.requirement_level = requirement_level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFair_principles() {
        return fair_principles;
    }

    public void setFair_principles(String fair_principles) {
        this.fair_principles = fair_principles;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGuidelines() {
        return guidelines;
    }

    public void setGuidelines(String guidelines) {
        this.guidelines = guidelines;
    }

    public String getInternal_error() {
        return internal_error;
    }

    public void setInternal_error(String internal_error) {
        this.internal_error = internal_error;
    }

    public long getPassed_records() {
        return passed_records;
    }

    public void setPassed_records(long passed_records) {
        this.passed_records = passed_records;
    }

    public long getFailed_records() {
        return failed_records;
    }

    public void setFailed_records(long failed_records) {
        this.failed_records = failed_records;
    }

    public String getRule_status() {
        return rule_status;
    }

    public void setRule_status(String rule_status) {
        this.rule_status = rule_status;
    }

    public boolean isHas_errors() {
        return has_errors;
    }

    public void setHas_errors(boolean has_errors) {
        this.has_errors = has_errors;
    }

    public boolean isHas_warnings() {
        return has_warnings;
    }

    public void setHas_warnings(boolean has_warnings) {
        this.has_warnings = has_warnings;
    }

    @Override
    public String toString() {
        return "SummaryResult{" +
                "rule_name='" + ruleName + '\'' +
                ", rule_weight=" + rule_weight +
                ", passed_records=" + passed_records +
                ", failed_records=" + failed_records +
                '}';
    }

}
