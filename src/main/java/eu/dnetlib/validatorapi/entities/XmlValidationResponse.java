package eu.dnetlib.validatorapi.entities;

import java.util.ArrayList;
import java.util.List;

public class XmlValidationResponse {
    String validationScore;
    String fairScore;
    List<RuleInfo> rules;
    List<RuleInfo> fairRules;

    List<SummaryResultWithIssueMessages> summaryResults = new ArrayList<>();

    public String getValidationScore() {
        return validationScore;
    }

    public void setValidationScore(String validationScore) {
        this.validationScore = validationScore;
    }

    public String getFairScore() {
        return fairScore;
    }

    public void setFairScore(String fairScore) {
        this.fairScore = fairScore;
    }

    public List<RuleInfo> getRules() {
        return rules;
    }

    public void setRules(List<RuleInfo> rules) {
        this.rules = rules;
    }

    public List<RuleInfo> getFairRules() {
        return fairRules;
    }

    public void setFairRules(List<RuleInfo> fairRules) {
        this.fairRules = fairRules;
    }

    public List<SummaryResultWithIssueMessages> getSummaryResults() {
        return summaryResults;
    }

    public void setSummaryResults(List<SummaryResultWithIssueMessages> summaryResults) {
        this.summaryResults = summaryResults;
    }
}
