package eu.dnetlib.validatorapi.entities;

import eu.dnetlib.validator2.engine.Status;

import java.util.List;

public class RuleInfo {
    String name;
    String description;
    List<String> warnings;
    List<String> errors;
    String internalError;
    Status status;
    int score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getInternalError() {
        return internalError;
    }

    public void setInternalError(String internalError) {
        this.internalError = internalError;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    //    public StandardResult getResult() {
//        return result;
//    }
//
//    public void setResult(StandardResult result) {
//        this.result = result;
//    }
}
