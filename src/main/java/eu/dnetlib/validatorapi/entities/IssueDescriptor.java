package eu.dnetlib.validatorapi.entities;

import java.util.ArrayList;
import java.util.List;

public class IssueDescriptor {
    private String description;
    private List<String> records = new ArrayList<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getRecords() {
        return records;
    }

    public void setRecords(List<String> records) {
        this.records = records;
    }
}
