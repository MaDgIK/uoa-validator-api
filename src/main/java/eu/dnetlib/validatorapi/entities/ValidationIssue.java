package eu.dnetlib.validatorapi.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="validation_issues")
@IdClass(ValidationIssue.class)
public class ValidationIssue implements Serializable {
    @Id
    @Column(name = "validation_job_id")
    public int validationJobId;

    @Id
    @Column(name = "rule_name")
    public String ruleName;

    @Id
    @Column(name = "record_url")
    public String recordUrl;

    @Id
    @Column(name = "issue_text")
    public String issueText;

    @Column(name = "issue_type")
    public String issueType;

    public ValidationIssue(){};

    public ValidationIssue(int validationJobId, String ruleName, String recordUrl, String issueText, String issueType) {
        this.validationJobId = validationJobId;
        this.ruleName = ruleName;
        this.recordUrl = recordUrl;
        this.issueText = issueText;
        this.issueType = issueType;
    }

    public ValidationIssue(String ruleName, String issueText, String issueType) {
        this.ruleName = ruleName;
        this.issueText = issueText;
        this.issueType = issueType;
    }

    public long getValidationJobId() {
        return validationJobId;
    }

    public void setValidationJobId(int validationJobId) {
        this.validationJobId = validationJobId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRecordUrl() {
        return recordUrl;
    }

    public void setRecordUrl(String recordUrl) {
        this.recordUrl = recordUrl;
    }

    public String getIssueText() {
        return issueText;
    }

    public void setIssueText(String issueText) {
        this.issueText = issueText;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    @Override
    public String toString() {
        return "ValidationIssue{" +
                "validationJobId=" + validationJobId +
                ", ruleName='" + ruleName + '\'' +
                ", recordUrl='" + recordUrl + '\'' +
                ", issueText='" + issueText + '\'' +
                ", issueType='" + issueType + '\'' +
                '}';
    }
}
