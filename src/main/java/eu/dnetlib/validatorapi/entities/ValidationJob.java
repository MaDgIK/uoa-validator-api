package eu.dnetlib.validatorapi.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "validation_jobs")
public class ValidationJob {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    public int id;

    @Column(name = "base_url")
    public String baseUrl;

    @Column(name = "set")
    public String set;

    @Column(name="number_of_records")
    public int numberOfRecords;

    @Column(name="guidelines")
    public String guidelines;

    @Column(name="started")
    public Date startDate;

    @Column(name="ended")
    public Date endDate;

    @Column(name = "records_tested")
    public int recordsTested;

    @Enumerated(EnumType.STRING)
    @Column(name="progress")
    public Progress progress; //stopped, completed, in progress

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    public Status status; //success, failure

    @Column(name="score")
    public double score;

    @Column(name="exception_class")
    public String exceptionClass;

    @Column(name="exception_message")
    public String exceptionMessage;


    public ValidationJob(){
        startDate = new Date();
    }

    public ValidationJob(String baseUrl, Optional<String> set, int numberOfRecords) {
        this.startDate = new Date();
        this.baseUrl = baseUrl;
        if (set.isPresent()) this.set = set.get();
        this.numberOfRecords = numberOfRecords;
        this.progress = Progress.IN_PROGRESS;
    }

    @Override
    public String toString() {
        return "ValidationJob{" +
                "id=" + id +
                ", baseUrl='" + baseUrl + '\'' +
                ", set='" + set + '\'' +
                ", numberOfRecords=" + numberOfRecords +
                ", guidelines='" + guidelines + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", recordsTested=" + recordsTested +
                ", progress='" + progress + '\'' +
                ", status='" + status + '\'' +
                ", score=" + score +
                ", exceptionClass='" + exceptionClass + '\'' +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}

