package eu.dnetlib.validatorapi.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="validation_results")
@IdClass(ValidationRuleResult.class)
public class ValidationRuleResult implements Serializable {

    @Id
    @Column(name = "validation_job_id")
    public int validationJobId;

    @Column(name = "guidelines")
    public String guidelines;

    @Id
    @Column(name = "rule_name")
    public String ruleName;

    @Id
    @Column(name = "rule_weight")
    public int ruleWeight;

    @Id
    @Column(name = "record_url")
    public String recordUrl;

    @Column(name="requirement_level")
    public String requirement_level;

    @Column(name="description")
    public String description;

    @Column(name="fair_principles")
    public String fair_principles;

    @Column(name="link")
    public String link;

    @Column(name = "internal_error")
    public String internalError;

    @Column(name = "status")
    public String status;

    @Column(name = "score")
    public double score;

    @Column(name = "has_errors")
    public boolean hasErrors;

    @Column(name = "hasWarnings")
    public boolean hasWarnings;

    public ValidationRuleResult() {}

    @Override
    public String toString() {
        return "ValidationRuleResult{" +
                "validationJobId=" + validationJobId +
                ", guidelines='" + guidelines + '\'' +
                ", ruleName='" + ruleName + '\'' +
                ", ruleWeight=" + ruleWeight +
                ", recordUrl='" + recordUrl + '\'' +
                ", requirement_level='" + requirement_level + '\'' +
                ", description='" + description + '\'' +
                ", fairPrinciples='" + fair_principles + '\'' +
                ", link='" + link + '\'' +
                ", internalError='" + internalError + '\'' +
                ", status='" + status + '\'' +
                ", score=" + score +
                ", hasErrors=" + hasErrors +
                ", hasWarnings=" + hasWarnings +
                '}';
    }
}

