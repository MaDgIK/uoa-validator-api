package eu.dnetlib.validatorapi.entities;

import java.util.ArrayList;
import java.util.List;

public class SummaryResultWithIssueMessages extends SummaryResult {
    public List<ValidationIssue> issues = new ArrayList<>();
}
