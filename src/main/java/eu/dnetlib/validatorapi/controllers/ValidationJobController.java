package eu.dnetlib.validatorapi.controllers;

import eu.dnetlib.validatorapi.entities.ValidationJob;
import eu.dnetlib.validatorapi.repositories.ValidationJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/jobs")
public class ValidationJobController {

    @Autowired
    ValidationJobRepository validationJobRepository;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public List<ValidationJob> getJobs(@RequestParam(name = "page", defaultValue = "0") int page,
                                       @RequestParam(name="limit", defaultValue = "10") int limit) {
        return validationJobRepository.getLatestValidationJobs(PageRequest.of(page, limit));
    }

    @RequestMapping(value = {"/latest"}, method = RequestMethod.GET)
    public List<ValidationJob> getLatestJobs(@RequestParam(name="limit", defaultValue = "10") int limit) {
        return validationJobRepository.getLatestValidationJobs(PageRequest.of(0, limit));
    }


}
