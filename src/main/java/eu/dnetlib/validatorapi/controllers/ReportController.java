package eu.dnetlib.validatorapi.controllers;

import eu.dnetlib.validatorapi.entities.IssueDescriptor;
import eu.dnetlib.validatorapi.entities.SummaryResult;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import eu.dnetlib.validatorapi.repositories.SummaryValidationJobRepository;
import eu.dnetlib.validatorapi.repositories.ValidationIssueRepository;
import eu.dnetlib.validatorapi.repositories.ValidationJobRepository;
import eu.dnetlib.validatorapi.repositories.ValidationResultRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/reports")
public class ReportController {
    private final ValidationJobRepository validationJobRepository;
    private final ValidationResultRepository validationResultRepository;
    private final ValidationIssueRepository validationIssueRepository;
    private final SummaryValidationJobRepository summaryValidationJobRepository;

    public ReportController(ValidationJobRepository validationJobRepository,
                            ValidationResultRepository validationResultRepository,
                            ValidationIssueRepository validationIssueRepository,
                            SummaryValidationJobRepository summaryValidationJobRepository) {
            this.validationJobRepository = validationJobRepository;
            this.validationResultRepository = validationResultRepository;
            this.validationIssueRepository = validationIssueRepository;
            this.summaryValidationJobRepository = summaryValidationJobRepository;
    }

    @RequestMapping(value={"getJobResult"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationJob getJobResults(@RequestParam(name = "jobId") int jobId){
        Optional<ValidationJob> validationJob = validationJobRepository.findById(jobId);
        return (ValidationJob) validationJob.orElse(null);
    }

    @RequestMapping(value = {"getResultsByJobId"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SummaryResult> getSummaryJobResults(@RequestParam(name = "jobId") int jobId,
                                                    @RequestParam(name= "guidelines", required = false) Optional<String> guidelines) {
        if (guidelines.isPresent() && guidelines.get().isEmpty())
            return validationResultRepository.getFullSummaryResult(jobId, guidelines.get());

        return validationResultRepository.getFullSummaryResult(jobId);
    }

    @RequestMapping(value = {"saveResultsByJobId"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveSummaryJobResults(@RequestParam(name = "jobId") int jobId,
                                                    @RequestParam(name= "guidelines", required = false) Optional<String> guidelines) {
        List<SummaryResult> summaryResultList;
        if (guidelines.isPresent() && guidelines.get().isEmpty())
            summaryResultList = validationResultRepository.getFullSummaryResult(jobId, guidelines.get());
        else
            summaryResultList = validationResultRepository.getFullSummaryResult(jobId);

        for(SummaryResult sr:summaryResultList){
            System.out.println("Saving " + sr.getRuleName() + ", " + sr.getValidationJobId());
            sr.setValidationJobId(jobId);
            summaryValidationJobRepository.save(sr);
        }
    }

    @RequestMapping(value = {"getSummaryFromDB"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SummaryResult> getSummaryFromDB(@RequestParam(name = "jobId") int jobId,
                                      @RequestParam(name= "guidelines", required = false) Optional<String> guidelines) {
        List<SummaryResult> srlist =  summaryValidationJobRepository.findByValidationJobIdOrderByRuleName(jobId);
        return srlist;

    }

    @RequestMapping(value = {"getWarningsReport"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    private List<IssueDescriptor> getWarningDescriptors(@RequestParam(name = "jobId") int jobId,
                                                      @RequestParam(name = "ruleName") String ruleName){
        List<Object[]> resultList = validationIssueRepository.getAllWarningsRecordUrlsByRuleName(jobId, ruleName);
        return extractIssueDescriptor(resultList);
    }


    @RequestMapping(value = {"getErrorsReport"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    private List<IssueDescriptor> getErrorDescriptors(@RequestParam(name = "jobId") int jobId,
                                                      @RequestParam(name = "ruleName") String ruleName){
        List<Object[]> resultList = validationIssueRepository.getAllErrorsRecordUrlsByRuleName(jobId, ruleName);
        return extractIssueDescriptor(resultList);
    }

    private List<IssueDescriptor> extractIssueDescriptor(List<Object[]> resultList) {
        List<IssueDescriptor> issueDescriptors = new ArrayList<>();

        for (Object[] result : resultList) {
            String issueText = (String) result[0];
            String recordUrl = (String) result[1];

            IssueDescriptor issueDescriptor = null;
            for (IssueDescriptor descriptor : issueDescriptors) {
                if (descriptor.getDescription().equals(issueText)) {
                    issueDescriptor = descriptor;
                    break;
                }
            }

            if (issueDescriptor == null) {
                issueDescriptor = new IssueDescriptor();
                issueDescriptor.setDescription(issueText);
                issueDescriptors.add(issueDescriptor);
            }

            issueDescriptor.getRecords().add(recordUrl);
        }

        return issueDescriptors;
    }

    /*
    @RequestMapping(value = {"getErrorsReport"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<String>> getErrors(@RequestParam(name = "jobId") int jobId, @RequestParam(name = "ruleName") String ruleName){
        List<Object[]> resultList = validationIssueRepository.getAllErrorsRecordUrlsByRuleName(jobId, ruleName);
        return extractRecordsGroupedByRule(resultList);
    }

    @RequestMapping(value = {"getWarningsReport"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<String>> getWarningsReport(@RequestParam(name = "jobId") int jobId, @RequestParam(name = "ruleName") String ruleName){
        List<Object[]> resultList = validationIssueRepository.getAllWarningsRecordUrlsByRuleName(jobId, ruleName);
        return extractRecordsGroupedByRule(resultList);
    }

    private Map<String, List<String>> extractRecordsGroupedByRule(List<Object[]> resultList) {
        Map<String, List<String>> recordUrlsByIssueText = new HashMap<>();

        for (Object[] result : resultList) {
            String issueText = (String) result[0];
            String recordUrl = (String) result[1];

            List<String> recordUrls = recordUrlsByIssueText.get(issueText);
            if (recordUrls == null) {
                recordUrls = new ArrayList<>();
                recordUrlsByIssueText.put(issueText, recordUrls);
            }
            recordUrls.add(recordUrl);
        }
        return recordUrlsByIssueText;
    }

    */
}