package eu.dnetlib.validatorapi.controllers;

import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.openaire.AbstractOpenAireProfile;
import eu.dnetlib.validator2.validation.guideline.openaire.DataArchiveGuidelinesV2Profile;
import eu.dnetlib.validator2.validation.guideline.openaire.LiteratureGuidelinesV3Profile;
import eu.dnetlib.validator2.validation.guideline.openaire.LiteratureGuidelinesV4Profile;
import eu.dnetlib.validatorapi.entities.*;
import eu.dnetlib.validatorapi.utils.ProfileInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class ValidatorController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @RequestMapping(value = {"/validate"}, method = RequestMethod.POST)
//    public List<String> validate(@RequestParam(name = "guidelines") String guidelinesProfileName, @RequestBody String xml) {
    public XmlValidationResponse validate(@RequestParam(name = "guidelines") String guidelinesProfileName, @RequestBody String xml) {
        log.debug(xml);
//        List<String> resultMessages = new ArrayList<>();
        List<RuleInfo> resultRules = null;
        List<RuleInfo> fairRules = null;
        XmlValidationResponse xmlValidationResponse = new XmlValidationResponse();

        AbstractOpenAireProfile profile =  ProfileInitializer.initializeOpenAireProfile(guidelinesProfileName);
        AbstractOpenAireProfile fairProfile = ProfileInitializer.initializeFairProfile(guidelinesProfileName);

        if(profile == null && fairProfile == null) {
            log.error("Exception: No valid guidelines");
            new Exception("No valid guidelines");
        }
        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
//        String file = "/home/konstantina/projects/validator-api/src/main/resources/Record_21811.xml";
        XMLApplicationProfile.ValidationResult result = null;
        try {
//            log.debug("Processing " + file);
//            Document doc = DOMBuilder.parse(new FileReader(file), false, true, true);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(xml)));

            if(profile != null) {
                resultRules = new ArrayList<>();

                log.debug("Max score: " + profile.maxScore());

                result = profile.validate("id", doc);
                Date date = new Date();
                scorePerDoc.put("input-" + date.toString(), result.getScore());
                Map<String, Guideline.Result> results = result.getResults();
                log.debug("\n\nPrinting results...");
                for (Map.Entry entry : results.entrySet()) {
                    log.debug(entry.getKey() + " = " + entry.getValue());
//                    resultMessages.add(entry.getKey() + " = " + entry.getValue());
                    RuleInfo ruleInfo = new RuleInfo();
                    ruleInfo.setName(entry.getKey().toString());

                    Guideline.Result res = (Guideline.Result) entry.getValue();
                    ruleInfo.setErrors((List<String>) res.getErrors());
                    ruleInfo.setWarnings((List<String>) res.getWarnings());
                    ruleInfo.setInternalError(res.getInternalError());
                    ruleInfo.setScore(res.getScore());
                    ruleInfo.setStatus(res.getStatus());

                    resultRules.add(ruleInfo);
                }
                String printout = scorePerDoc.entrySet().stream().
                        map(entry -> {
                            xmlValidationResponse.setValidationScore(entry.getValue().toString());

//                            resultMessages.add("score: " + entry.getValue());
                            return entry.getValue() + ": " + entry.getKey();
                        }).collect(Collectors.joining("\n"));
                log.debug("\n\nPrinting scorePerDoc...");
                log.debug(printout);
            }

            if(fairProfile != null) {
                fairRules = new ArrayList<>();

                log.debug("Max score: " + fairProfile.maxScore());

                result = fairProfile.validate("id", doc);
                Date date = new Date();
                scorePerDoc.put("input-"+date.toString(), result.getScore());
                Map<String, Guideline.Result> results = result.getResults();
                log.debug("\n\nPrinting FAIR results...");
                for (Map.Entry entry : results.entrySet()) {
//                    if (!entry.getValue().toString().contains("eu.dnetlib.validator2")) {
                        log.debug(entry.getKey() + " = " + entry.getValue());
//                        resultMessages.add(entry.getKey() + " = " + entry.getValue());
                        RuleInfo ruleInfo = new RuleInfo();
                        ruleInfo.setName(entry.getKey().toString());

                        Guideline.Result res = (Guideline.Result) entry.getValue();
                        ruleInfo.setErrors((List<String>) res.getErrors());
                        ruleInfo.setWarnings((List<String>) res.getWarnings());
                        ruleInfo.setInternalError(res.getInternalError());
                        ruleInfo.setScore(res.getScore());
                        ruleInfo.setStatus(res.getStatus());

                        fairRules.add(ruleInfo);
//                    }
                }
                String printout = scorePerDoc.entrySet().stream().
                        map(entry -> {
                            xmlValidationResponse.setFairScore(entry.getValue().toString());

//                            resultMessages.add("fair score: " + entry.getValue());
                            return entry.getValue() + ": " + entry.getKey();
                        }).collect(Collectors.joining("\n"));
                log.debug("\n\nPrinting fair scorePerDoc...");
                log.debug(printout);
            }
        }
        catch(Exception e) {
            log.debug(e.getMessage());
            log.debug(e);
            e.printStackTrace();
//            resultMessages.add("Error processing input");
        }

//        return resultMessages;
        xmlValidationResponse.setRules(resultRules);
        xmlValidationResponse.setFairRules(fairRules);
        return xmlValidationResponse;
    }


    @RequestMapping(value = {"/validateRecord"}, method = RequestMethod.POST)
    public XmlValidationResponse validateRecord(@RequestParam(name = "guidelines") String guidelinesProfileName, @RequestBody String xml) {

        XmlValidationResponse xmlValidationResponse = new XmlValidationResponse();
        AbstractOpenAireProfile profile =  ProfileInitializer.initializeOpenAireProfile(guidelinesProfileName);
        AbstractOpenAireProfile fairProfile = ProfileInitializer.initializeFairProfile(guidelinesProfileName);


        if(profile == null && fairProfile == null) {
            log.error("Exception: No valid guidelines");
            new Exception("Validation stopped unexpectedly. No valid guidelines were provided.");
        }

        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
        List<ValidationIssue> validationIssues = new ArrayList<>();
        List<SummaryResult> summaryResults = new ArrayList<>();

        try {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            final DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document doc = documentBuilder.parse(new InputSource(new StringReader(xml)));

            XMLApplicationProfile.ValidationResult validationResult;
            Map<String, Guideline.Result> results;

            if (profile != null) {
                validationResult = profile.validate("id", doc); //what id is that?
                results = validationResult.getResults();

                for (Map.Entry entry : results.entrySet()) { //for each rule of the record
                    final Guideline.Result engineResult = (Guideline.Result) entry.getValue();
                    String ruleName = entry.getKey().toString();
                    ValidationRuleResult validationRuleResult = new ValidationRuleResult();
                    constructXmlValidationResponse(xmlValidationResponse, validationRuleResult, profile.name(),
                            ruleName, profile, engineResult);

                    //TODO check if I want a global status
                    /*if (validationRuleResult.status.equals("FAILURE") || validationRuleResult.internalError!=null) {
                        this.status = "FAILURE";
                    }*/

                }
            }

            if (fairProfile != null) {
                validationResult = fairProfile.validate("id", doc); //what id is that?
                results = validationResult.getResults();

                for (Map.Entry entry : results.entrySet()) { //for each rule of the record
                    final Guideline.Result engineResult = (Guideline.Result) entry.getValue();
                    String ruleName = entry.getKey().toString();
                    ValidationRuleResult validationRuleResult = new ValidationRuleResult();
                    constructXmlValidationResponse(xmlValidationResponse, validationRuleResult, profile.name(),
                            ruleName, profile, engineResult);

                    //TODO check if I want a global failure
                /*if (validationRuleResult.status.equals("FAILURE") || validationRuleResult.internalError!=null) {
                    this.status = "FAILURE";
                }*/

                }
            }
        }
        catch(Exception e) {
            log.debug(e.getMessage());
            log.debug(e);
            e.printStackTrace();
//            resultMessages.add("Error processing input");
        }

//        return resultMessages;

        return xmlValidationResponse;
    }




    @RequestMapping(method = RequestMethod.POST, value = {"/validate-file"})
    public  List<String> validateXmlImport(@RequestParam("guidelines") String guidelinesProfileName, @RequestParam("file") MultipartFile multipartFile) throws IOException {
        String filePath = "src/main/resources/xmlFile.tmp";
        File file = new File(filePath);
        multipartFile.transferTo(file);

        List<String> resultMessages = new ArrayList<>();

        AbstractOpenAireProfile profile = null;
        if(guidelinesProfileName.equals("dataArchiveGuidelinesV2Profile")) {
            profile = new DataArchiveGuidelinesV2Profile();
        } else if(guidelinesProfileName.equals("literatureGuidelinesV3Profile")) {
            profile = new LiteratureGuidelinesV3Profile();
        } else if(guidelinesProfileName.equals("literatureGuidelinesV4Profile")) {
            profile = new LiteratureGuidelinesV4Profile();
        }
        if(profile == null) {
            new Exception("No valid guidelines");
        }
        log.debug("Max score: " + profile.maxScore());
        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
//        String file = "/home/konstantina/projects/validator-api/src/main/resources/Record_21811.xml";
        XMLApplicationProfile.ValidationResult result = null;
        try {
//            log.debug("Processing " + file);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
//            Document doc = db.parse(new InputSource(new StringReader(xml)));
            Document doc = db.parse(filePath);
            result = profile.validate("id", doc);
            Date date = new Date();
            scorePerDoc.put("input-"+date.toString(), result.getScore());
            Map<String, Guideline.Result> results = result.getResults();
            log.debug("\n\nPrinting results...");
            for (Map.Entry entry : results.entrySet()) {
                log.debug(entry.getKey() + " = " + entry.getValue());
                resultMessages.add(entry.getKey() + " = " + entry.getValue());
            }
        }
        catch(Exception e) {
            log.debug(e.getMessage());
            log.debug(e);
            e.printStackTrace();
            resultMessages.add("Error processing input");
        }
        String printout = scorePerDoc.entrySet().stream().
                map(entry -> {
                    resultMessages.add("score: " + entry.getValue());
                    return entry.getValue() + ": " + entry.getKey();
                }).collect(Collectors.joining("\n"));
        log.debug("\n\nPrinting scorePerDoc...");
        log.debug(printout);

        Files.deleteIfExists(file.toPath());

        return resultMessages;

    }

    private XmlValidationResponse constructXmlValidationResponse(XmlValidationResponse xmlValidationResponse, ValidationRuleResult validationRuleResult, String guidelines,
                                                                 String ruleName, AbstractOpenAireProfile profile, Guideline.Result engineResult) {
        SummaryResultWithIssueMessages summaryResult = new SummaryResultWithIssueMessages();
        summaryResult.ruleName = ruleName;
        summaryResult.validationJobId = -1;
        summaryResult.guidelines = guidelines;
        validationRuleResult.score = engineResult.getScore();
        summaryResult.rule_status = engineResult.getStatus().toString();
        summaryResult.passed_records = 1;

        if (profile.guideline(ruleName) != null) {
            summaryResult.rule_weight = profile.guideline(ruleName).getWeight();

            if (profile.guideline(ruleName).getRequirementLevel() != null
                    && profile.guideline(ruleName).getRequirementLevel().name()!=null)
                summaryResult.requirement_level = profile.guideline(ruleName).getRequirementLevel().name();

            if (profile.guideline(ruleName).getDescription() != null)
                summaryResult.description = profile.guideline(ruleName).getDescription();

            if (profile.guideline(ruleName).getFairPrinciples() != null)
                summaryResult.fair_principles = profile.guideline(ruleName).getFairPrinciples();

            if (profile.guideline(ruleName).getLink() != null)
                summaryResult.link = profile.guideline(ruleName).getLink();
        }
        summaryResult.internal_error = engineResult.getInternalError();
        summaryResult.has_warnings = (engineResult.getWarnings().size()>0);
        summaryResult.has_errors = (engineResult.getErrors().size()>0);

        xmlValidationResponse.getSummaryResults().add(summaryResult);

        for (String error: engineResult.getErrors()) {
           ValidationIssue validationIssue = new ValidationIssue(ruleName, error, "ERROR");
           summaryResult.issues.add(validationIssue);
        }

        for (String warning: engineResult.getWarnings()) {
            ValidationIssue validationIssue = new ValidationIssue(ruleName, warning, "WARNING");
            summaryResult.issues.add(validationIssue);
        }

        return xmlValidationResponse;
    }
}
