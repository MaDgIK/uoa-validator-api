package eu.dnetlib.validatorapi.controllers;

import java.io.Serializable;

public class SummaryResultId implements Serializable {
    public String ruleName;
    public int validationJobId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SummaryResultId that = (SummaryResultId) o;

        if (validationJobId != that.validationJobId) return false;
        return ruleName != null ? ruleName.equals(that.ruleName) : that.ruleName == null;
    }

    // Implement hashCode method
    @Override
    public int hashCode() {
        int result = ruleName != null ? ruleName.hashCode() : 0;
        result = 31 * result + validationJobId;
        return result;
    }
}
