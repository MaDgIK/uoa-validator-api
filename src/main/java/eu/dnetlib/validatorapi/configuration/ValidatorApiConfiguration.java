package eu.dnetlib.validatorapi.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ValidatorApiConfiguration implements WebMvcConfigurer {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Bean
    public static PropertySourcesPlaceholderConfigurer  propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
