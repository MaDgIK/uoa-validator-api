package eu.dnetlib.validatorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.validatorapi"})
@PropertySources({
        @PropertySource("classpath:validatorapi.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})

public class ValidatorApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ValidatorApiApplication.class, args);
    }
}