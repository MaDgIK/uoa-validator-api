package eu.dnetlib.validatorapi.repositories;

import eu.dnetlib.validatorapi.entities.ValidationJob;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ValidationJobRepository extends JpaRepository<ValidationJob, Integer> {
    @Query(value ="SELECT vj FROM ValidationJob vj ORDER BY vj.startDate DESC")
    List<ValidationJob> getLatestValidationJobs(Pageable pageable);
}
