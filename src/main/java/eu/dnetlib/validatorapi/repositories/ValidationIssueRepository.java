package eu.dnetlib.validatorapi.repositories;

import eu.dnetlib.validatorapi.entities.ValidationIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ValidationIssueRepository extends JpaRepository<ValidationIssue, Long> {


    @Query(value = "SELECT vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'WARNING' AND vi.validationJobId =:id AND vi.ruleName =:ruleName)" +
            "GROUP BY vi.issueText, vi.recordUrl")
    List<ValidationIssue> getWarnings(@Param("id") int validationJobId, @Param("ruleName") String ruleName);

    @Query(value = "SELECT vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'ERROR'  AND vi.validationJobId =:id AND vi.ruleName =:ruleName)" +
            "GROUP BY vi.issueText, vi.recordUrl")
    List<ValidationIssue> getErrors(@Param("id") int validationJobId, @Param("ruleName") String ruleName);

    @Query(value = "SELECT vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'ERROR' AND vi.validationJobId =:id AND vi.ruleName=:ruleName)" +
            "GROUP BY vi.issueText, vi.recordUrl")
    List<Object[]> getAllErrorsRecordUrlsByRuleName(@Param("id") int validationJobId, @Param("ruleName") String ruleName);

    @Query(value = "SELECT vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'WARNING' AND vi.validationJobId =:id AND vi.ruleName=:ruleName)" +
            "GROUP BY vi.issueText, vi.recordUrl")
    List<Object[]> getAllWarningsRecordUrlsByRuleName(@Param("id") int validationJobId, @Param("ruleName") String ruleName);

    @Query(value = "SELECT vi.ruleName, vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'ERROR' AND vi.validationJobId =:id)" +
            "GROUP BY vi.ruleName, vi.issueText, vi.recordUrl")
    List<Object[]> getAllErrors(@Param("id") int validationJobId);

    @Query(value = "SELECT vi.ruleName, vi.issueText, vi.recordUrl " +
            "FROM ValidationIssue vi " +
            "WHERE (vi.issueType = 'WARNING' AND vi.validationJobId =:id)" +
            "GROUP BY vi.ruleName, vi.issueText, vi.recordUrl")
    List<Object[]> getAllWarnings(@Param("id") int validationJobId);

}
