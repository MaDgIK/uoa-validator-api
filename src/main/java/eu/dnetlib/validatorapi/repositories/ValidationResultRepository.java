package eu.dnetlib.validatorapi.repositories;

import eu.dnetlib.validatorapi.entities.SummaryResult;
import eu.dnetlib.validatorapi.entities.ValidationRuleResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ValidationResultRepository extends JpaRepository<ValidationRuleResult, Integer> {

    @Query(value =
            "SELECT NEW eu.dnetlib.validatorapi.entities.SummaryResult(sr.ruleName, sr.ruleWeight," +
            "COUNT(CASE WHEN sr.status = 'SUCCESS' THEN 1 END) AS passed_records, " +
            "COUNT(CASE WHEN sr.status = 'FAILURE' THEN 1 END) AS failed_records) " +
            "FROM ValidationRuleResult sr " + "WHERE sr.validationJobId = :id " +
            "GROUP BY sr.ruleName, sr.ruleWeight")
    List<SummaryResult> getSummaryResult(@Param("id") int validationJobId);

    @Query("SELECT CASE WHEN COUNT(vr) > 0 THEN 'FAILURE' ELSE 'SUCCESS' END " +
            "FROM ValidationRuleResult vr " +
            "WHERE vr.validationJobId = :id AND vr.status = 'FAILURE' OR vr.internalError != null")
    public String getStatus(@Param("id") int validationJobId);

    @Query(value = "SELECT NEW eu.dnetlib.validatorapi.entities.SummaryResult(sr.ruleName, sr.ruleWeight, sr.requirement_level, " +
            "sr.description, sr.fair_principles, sr.link, sr.guidelines, sr.internalError," +
            "COUNT(CASE WHEN sr.status = 'SUCCESS' THEN 1 END) AS passedRecords," +
            "COUNT(CASE WHEN sr.status = 'FAILURE' OR sr.status = 'ERROR' THEN 1 END) AS failedRecords," +
            "(SELECT CASE" +
            " WHEN COUNT(DISTINCT CASE WHEN vr.status = 'ERROR' THEN vr.ruleName END) = COUNT(DISTINCT vr.ruleName) THEN 'ERROR'" +
            " WHEN COUNT(DISTINCT CASE WHEN vr.status = 'FAILURE' THEN vr.ruleName END) = COUNT(DISTINCT vr.ruleName) THEN 'FAILURE'" +
            " ELSE 'SUCCESS'" +
            " END " +
            "FROM eu.dnetlib.validatorapi.entities.ValidationRuleResult vr WHERE vr.ruleName = sr.ruleName and sr.validationJobId=vr.validationJobId)," +
            "CASE WHEN COUNT(CASE WHEN has_errors IS TRUE THEN 1 END) > 0 THEN TRUE ELSE FALSE END AS hasErrors," +
            "CASE WHEN COUNT(CASE WHEN has_warnings IS TRUE THEN 1 END) > 0 THEN TRUE ELSE FALSE END AS hasWarnings)" +
            "FROM eu.dnetlib.validatorapi.entities.ValidationRuleResult sr " +
            "WHERE sr.validationJobId = :id " +
            "GROUP BY sr.ruleName, sr.ruleWeight, sr.description, sr.fair_principles, sr.link, sr.requirement_level, sr.guidelines,  sr.internalError, sr.validationJobId " +
            "ORDER BY sr.ruleName")
    List<SummaryResult> getFullSummaryResult(@Param("id") int validationJobId);

    @Query(value =
            "SELECT NEW eu.dnetlib.validatorapi.entities.SummaryResult(sr.ruleName, sr.ruleWeight," +
                    "COUNT(CASE WHEN sr.status = 'SUCCESS' THEN 1 END) AS passed_records, " +
                    "COUNT(CASE WHEN sr.status = 'FAILURE' THEN 1 END) AS failed_records) " +
                    "FROM ValidationRuleResult sr " + "WHERE sr.validationJobId = :id and sr.guidelines = :guidelines " +
                    "GROUP BY sr.ruleName, sr.ruleWeight")
    List<SummaryResult> getSummaryResult(@Param("id") int validationJobId, @Param("guidelines") String guidelines);

    @Query("SELECT CASE WHEN COUNT(vr) > 0 THEN 'FAILURE' ELSE 'SUCCESS' END " +
            "FROM ValidationRuleResult vr " +
            "WHERE vr.validationJobId = :id AND vr.guidelines = :guidelines AND vr.status = 'FAILURE' OR vr.internalError != null")
    public String getStatus(@Param("id") int validationJobId, @Param("guidelines") String guidelines);

    @Query(value = "SELECT NEW eu.dnetlib.validatorapi.entities.SummaryResult(sr.ruleName, sr.ruleWeight, sr.requirement_level, " +
            "sr.description, sr.fair_principles, sr.link, sr.guidelines, sr.internalError," +
            "COUNT(CASE WHEN sr.status = 'SUCCESS' THEN 1 END) AS passedRecords," +
            "COUNT(CASE WHEN sr.status = 'FAILURE' OR sr.status = 'ERROR' THEN 1 END) AS failedRecords," +
            "(SELECT CASE" +
            " WHEN COUNT(DISTINCT CASE WHEN vr.status = 'ERROR' THEN vr.ruleName END) = COUNT(DISTINCT vr.ruleName) THEN 'ERROR'" +
            " WHEN COUNT(DISTINCT CASE WHEN vr.status = 'FAILURE' THEN vr.ruleName END) = COUNT(DISTINCT vr.ruleName) THEN 'FAILURE'" +
            " ELSE 'SUCCESS'" +
            " END " +
            "FROM eu.dnetlib.validatorapi.entities.ValidationRuleResult vr WHERE vr.ruleName = sr.ruleName and sr.validationJobId=vr.validationJobId)," +
            "CASE WHEN COUNT(CASE WHEN has_errors IS TRUE THEN 1 END) > 0 THEN TRUE ELSE FALSE END AS hasErrors," +
            "CASE WHEN COUNT(CASE WHEN has_warnings IS TRUE THEN 1 END) > 0 THEN TRUE ELSE FALSE END AS hasWarnings)" +
            "FROM eu.dnetlib.validatorapi.entities.ValidationRuleResult sr " +
            "WHERE sr.validationJobId = :id AND sr.guidelines = :guidelines " +
            "GROUP BY sr.ruleName, sr.ruleWeight, sr.requirement_level, sr.description, sr.fair_principles, sr.link, " +
            "sr.description, sr.internalError, sr.validationJobId, sr.guidelines")
    List<SummaryResult> getFullSummaryResult(@Param("id") int validationJobId, @Param("guidelines") String guidelines);


    @Modifying
    @Transactional
    @Query("DELETE FROM ValidationRuleResult vr WHERE vr.validationJobId = :validationJobId")
    void customDelete(int validationJobId);

    @Modifying
    @Transactional
    @Query("DELETE FROM ValidationRuleResult vr WHERE vr.validationJobId = :validationJobId and vr.guidelines =:guidelines")
    void customDeleteByGuidelines(int validationJobId, String guidelines);
}
