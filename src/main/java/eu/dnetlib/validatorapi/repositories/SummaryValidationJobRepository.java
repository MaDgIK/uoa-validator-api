package eu.dnetlib.validatorapi.repositories;

import eu.dnetlib.validatorapi.entities.SummaryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SummaryValidationJobRepository extends JpaRepository<SummaryResult, Integer> {

    List<SummaryResult> findByValidationJobIdOrderByRuleName(int validationJobId);
}
