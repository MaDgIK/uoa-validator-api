package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validatorapi.entities.SummaryResult;
import eu.dnetlib.validatorapi.repositories.SummaryValidationJobRepository;
import eu.dnetlib.validatorapi.repositories.ValidationResultRepository;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.List;

//TODO:delete
public class SummaryValidationJobProcessor implements Processor {

    private final ValidationResultRepository validationResultRepository;
    private final SummaryValidationJobRepository summaryValidationJobRepository;
    private final String routeId;

    public SummaryValidationJobProcessor(ValidationResultRepository validationResultRepository,
                                  SummaryValidationJobRepository summaryValidationJobRepository,
                                         String routeId){
        this.validationResultRepository = validationResultRepository;
        this.summaryValidationJobRepository = summaryValidationJobRepository;
        this.routeId = routeId;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        int validation_job_id = (Integer) exchange.getIn().getHeader("validationId");
        List<SummaryResult> summaryResults  = validationResultRepository.getFullSummaryResult
                ((Integer) exchange.getIn().getHeader("validationId"));

        for(SummaryResult sr: summaryResults) {
            sr.validationJobId = validation_job_id;
            summaryValidationJobRepository.save(sr);
        }

        validationResultRepository.customDelete(validation_job_id);

        exchange.getIn().setHeader("CamelControlBusAction", "stop");
        exchange.getIn().setHeader("CamelControlBusRouteId", routeId);
    }
}
