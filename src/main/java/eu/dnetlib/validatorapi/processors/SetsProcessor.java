package eu.dnetlib.validatorapi.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.support.DefaultMessage;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SetsProcessor  implements Processor {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        List<DefaultMessage> messages = exchange.getIn().getBody(ArrayList.class);

        //If the body is null a timeout has occurred.
        if (exchange.getIn().getBody() == null)
            throw new TimeoutException();

        List<String> ListSetsXml = exchange.getIn().getBody(ArrayList.class);
        List<String> desiredJsonParts = new ArrayList<>();
        String xmlSets = "";

        // Extract the desired <set> elements from each XML string
        for (String xml : ListSetsXml) {
            List<String> extractedSets = extractSets(xml);
            for (String setXml : extractedSets)
                xmlSets += setXml;
        }

        JSONObject jsonObject = XML.toJSONObject(xmlSets);
        String jsonString = jsonObject.toString();
        exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_OK);
        exchange.getIn().setBody(jsonString);
    }

    private List<String> extractSets(String xml) {
        List<String> setXmls = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(new StringReader(xml));
            Document document = builder.parse(inputSource);

            NodeList setElements = document.getElementsByTagName("set");
            for (int i = 0; i < setElements.getLength(); i++) {
                Node setElement = setElements.item(i);
                String setXml = nodeToString(setElement);
                setXmls.add(setXml);
            }
        } catch (Exception e) {
            // TODO: consider if Ineed to handle any exceptions that occur during
            //  XML parsing or processing and not only log them
           log.error("Could not parse the xml ", e);
        }

        return setXmls;
    }

    private String nodeToString(Node node) throws TransformerException {
        StringWriter sw = new StringWriter();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(node), new StreamResult(sw));
        return sw.toString();
    }
}
