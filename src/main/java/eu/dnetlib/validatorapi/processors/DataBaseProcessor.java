package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validatorapi.entities.Progress;
import eu.dnetlib.validatorapi.entities.Status;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.Date;

public class DataBaseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        ValidationJob validationJob = new ValidationJob();
        validationJob.id = (Integer)exchange.getIn().getHeader("validationId");
        validationJob.baseUrl = (String)(exchange.getIn().getHeader("baseUrl"));
        validationJob.set = (String)(exchange.getIn().getHeader("set"));
        validationJob.startDate = (Date) exchange.getIn().getHeader("startDate");
        validationJob.guidelines = (String) exchange.getIn().getHeader("guidelines");
        validationJob.status = Status.valueOf(((String)exchange.getIn().getHeader("status")).toUpperCase());
        validationJob.recordsTested = (Integer) exchange.getIn().getHeader("recordsTested");
        validationJob.numberOfRecords = Math.toIntExact((Long) exchange.getIn().getHeader("maxNumberOfRecords"));
        validationJob.score = (Integer) exchange.getIn().getHeader("score");
        validationJob.progress = Progress.COMPLETED;
        validationJob.endDate = new Date();

        exchange.getIn().setBody(validationJob, ValidationJob.class);
    }
}
