package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validatorapi.entities.Progress;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.Date;

public class ExceptionProcessor implements Processor {

    ValidationJob validationJob;

    public ExceptionProcessor(ValidationJob validationJob) {
        this.validationJob = validationJob;
    }

    @Override
    public void process(Exchange exchange) {
        Throwable caused = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
        if (caused.getClass() != null) {
            if (caused.getClass().getName() != null) {
                validationJob.exceptionClass = caused.getClass().getName();
            }
        }

        if (caused.getCause() != null) {
            if (caused.getCause().getMessage() != null) {
                validationJob.exceptionMessage = caused.getCause().toString();
            }
        }

        validationJob.progress = Progress.STOPPED;
        validationJob.endDate = new Date();

        exchange.getIn().setBody(validationJob, ValidationJob.class);
    }
}
