package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validator2.validation.guideline.openaire.AbstractOpenAireProfile;
import eu.dnetlib.validatorapi.entities.Progress;
import eu.dnetlib.validatorapi.entities.Status;
import eu.dnetlib.validatorapi.entities.SummaryResult;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import eu.dnetlib.validatorapi.repositories.SummaryValidationJobRepository;
import eu.dnetlib.validatorapi.repositories.ValidationJobRepository;
import eu.dnetlib.validatorapi.repositories.ValidationResultRepository;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

public class SumUpNCleanProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(SumUpNCleanProcessor.class);
    private final ValidationJobRepository validationJobRepository;
    private final ValidationResultRepository validationResultRepository;
    private final SummaryValidationJobRepository summaryValidationJobRepository;
    private final AbstractOpenAireProfile profile;

    public SumUpNCleanProcessor(ValidationJobRepository validationJobRepository,
                                ValidationResultRepository validationResultRepository,
                                SummaryValidationJobRepository summaryValidationJobRepository,
                                AbstractOpenAireProfile profile) {
        this.validationJobRepository = validationJobRepository;
        this.validationResultRepository = validationResultRepository;
        this.summaryValidationJobRepository = summaryValidationJobRepository;
        this.profile = profile;
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        ValidationJob validationJob = new ValidationJob();
        validationJob.id = (Integer)exchange.getIn().getHeader("validationId");
        validationJob.baseUrl = (String)(exchange.getIn().getHeader("baseUrl"));
        validationJob.set = (String)(exchange.getIn().getHeader("set"));
        validationJob.startDate = (Date) exchange.getIn().getHeader("startDate");
        validationJob.guidelines = (String) exchange.getIn().getHeader("guidelines");
        validationJob.status = Status.valueOf(((String)exchange.getIn().getHeader("status")).toUpperCase());
        validationJob.recordsTested = (Integer) exchange.getIn().getHeader("recordsTested");
        validationJob.numberOfRecords = Math.toIntExact((Long) exchange.getIn().getHeader("maxNumberOfRecords"));
        validationJob.score = (Integer) exchange.getIn().getHeader("score");
        validationJob.progress = Progress.COMPLETED;
        validationJob.endDate = new Date();

        List<SummaryResult> summaryResults  = validationResultRepository.getFullSummaryResult
                ((Integer) exchange.getIn().getHeader("validationId"), profile.name());

        for(SummaryResult sr: summaryResults) {
            sr.validationJobId = validationJob.id;
            summaryValidationJobRepository.save(sr);
        }

        validationResultRepository.customDeleteByGuidelines(validationJob.id, profile.name());
        validationJobRepository.save(validationJob);

    }

}
