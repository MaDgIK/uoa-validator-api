package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validatorapi.entities.Progress;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.Date;

public class ErrorProcessor implements Processor {

    ValidationJob validationJob;

    public ErrorProcessor(ValidationJob validationJob) {
        this.validationJob = validationJob;
    }

    @Override
    public void process(Exchange exchange) {
        validationJob.exceptionMessage = "OAI-PMH Response is not valid. XML does not contain <record> element: " +
                exchange.getIn().getBody(String.class);

        validationJob.progress = Progress.STOPPED;
        validationJob.endDate = new Date();

        exchange.getIn().setBody(validationJob, ValidationJob.class);
    }

}
