package eu.dnetlib.validatorapi.processors;

import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.openaire.AbstractOpenAireProfile;
import eu.dnetlib.validatorapi.entities.ValidationIssue;
import eu.dnetlib.validatorapi.entities.ValidationJob;
import eu.dnetlib.validatorapi.entities.ValidationRuleResult;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(XmlProcessor.class);

    private final AbstractOpenAireProfile profile;
    private ValidationJob validationJob;
    private final long maxNumberOfRecords;
    private int processedRecords;
    private String status = "SUCCESS";
    private int scoreSum;


    public XmlProcessor(final AbstractOpenAireProfile profile, ValidationJob validationJob,
                        final long maxNumberOfRecords){
        super();
        this.profile = profile;
        this.validationJob = validationJob;
        this.maxNumberOfRecords = maxNumberOfRecords;
    }


    @Override
    public void process(Exchange exchange) throws Exception {
        String recordXml = exchange.getIn().getBody(String.class);

        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        final DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
        Document doc = documentBuilder.parse(new InputSource(new StringReader(recordXml)));
        String recordUrl = extractRecordUrl(doc, "identifier");

        List<ValidationRuleResult> validationRuleResults = new ArrayList<>();
        List<ValidationIssue> validationIssues = new ArrayList<>();

        if (profile != null) {
            XMLApplicationProfile.ValidationResult validationResult = profile.validate("id", doc); //what id is that?
            Map<String, Guideline.Result> results = validationResult.getResults();

            ValidationRuleResult validationRuleResult;
            for (Map.Entry entry : results.entrySet()) { //for each rule of the record
                final Guideline.Result engineResult = (Guideline.Result) entry.getValue();
                String ruleName = entry.getKey().toString();
                validationRuleResult = new ValidationRuleResult();
                constructValidationRuleResult(validationRuleResult, validationJob.id, profile.name(), recordUrl,
                        ruleName, profile, engineResult);

                if (validationRuleResult.status.equals("FAILURE") || validationRuleResult.internalError!=null) {
                    this.status = "FAILURE";
                }

                if (engineResult.getErrors()!= null && engineResult.getErrors().iterator().hasNext()) {
                    validationRuleResult.hasErrors = true;
                    for (String error:engineResult.getErrors()) {
                        validationIssues.add(new ValidationIssue(validationJob.id, ruleName, recordUrl, error,"ERROR"));
                    }
                }

                if (engineResult.getWarnings()!= null && engineResult.getWarnings().iterator().hasNext()) {
                    validationRuleResult.hasWarnings = true;
                    for (String warning:engineResult.getWarnings()) {
                        validationIssues.add(new ValidationIssue(validationJob.id, ruleName, recordUrl, warning, "WARNING"));
                    }
                }

                validationRuleResults.add(validationRuleResult);
                scoreSum += engineResult.getScore();
            }

        }

        processedRecords++;
        validationJob.recordsTested = processedRecords;
        validationJob.score = scoreSum/processedRecords;
        Map<String, List> resultsMap = new HashMap();
        resultsMap.put("results", validationRuleResults);
        resultsMap.put("issues", validationIssues);
        exchange.getIn().setBody(resultsMap, Map.class);

        if (processedRecords > maxNumberOfRecords) {
            exchange.getIn().setHeader("MyHeader", "stop");
            exchange.getIn().setHeader("validationId", validationJob.id);
            exchange.getIn().setHeader("baseUrl", validationJob.baseUrl);
            exchange.getIn().setHeader("set", validationJob.set);
            exchange.getIn().setHeader("startDate", validationJob.startDate);
            exchange.getIn().setHeader("guidelines", validationJob.guidelines);
            exchange.getIn().setHeader("status", status);
            exchange.getIn().setHeader("recordsTested", processedRecords-1);
            exchange.getIn().setHeader("maxNumberOfRecords", maxNumberOfRecords);
            exchange.getIn().setHeader("score", scoreSum/processedRecords);
            //exchange.setRouteStop(true);
        }
    }

    //TODO consider throwing exception - UTIL class?
    private String extractRecordUrl(final Document document, final String xmlField) {
        NodeList identifierNodes = document.getElementsByTagName(xmlField);
        if (identifierNodes.getLength() > 0) {
            Element identifierElement = (Element) identifierNodes.item(0);
            return identifierElement.getTextContent();
        }
        return "-";
    }


    private void constructValidationRuleResult(ValidationRuleResult validationRuleResult, int validationJobId,
                                               String guidelines, String recordUrl, String ruleName,
                                               AbstractOpenAireProfile profile, Guideline.Result engineResult) {

        validationRuleResult.validationJobId = validationJobId;
        validationRuleResult.guidelines = guidelines;
        validationRuleResult.ruleName = ruleName;

        if (profile.guideline(ruleName) != null) {
            validationRuleResult.ruleWeight = profile.guideline(ruleName).getWeight();

            if (profile.guideline(ruleName).getRequirementLevel() != null
                    && profile.guideline(ruleName).getRequirementLevel().name()!=null)
                        validationRuleResult.requirement_level = profile.guideline(ruleName).getRequirementLevel().name();

            if (profile.guideline(ruleName).getDescription() != null)
                validationRuleResult.description = profile.guideline(ruleName).getDescription();

            if (profile.guideline(ruleName).getFairPrinciples() != null)
                validationRuleResult.fair_principles = profile.guideline(ruleName).getFairPrinciples();

            if (profile.guideline(ruleName).getLink() != null)
                validationRuleResult.link = profile.guideline(ruleName).getLink();
        }

        validationRuleResult.recordUrl = recordUrl;
        validationRuleResult.score = engineResult.getScore();
        validationRuleResult.status = engineResult.getStatus().toString();
        validationRuleResult.internalError = engineResult.getInternalError();
    }
}
