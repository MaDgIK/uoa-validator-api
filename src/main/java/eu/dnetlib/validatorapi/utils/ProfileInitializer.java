package eu.dnetlib.validatorapi.utils;

import eu.dnetlib.validator2.validation.guideline.openaire.*;

public class ProfileInitializer {

    public static AbstractOpenAireProfile initializeOpenAireProfile(String guidelinesProfileName) {

        if (guidelinesProfileName.equals("OpenAIRE Guidelines for Data Archives Profile v2")) {
            return new DataArchiveGuidelinesV2Profile();

        } else if (guidelinesProfileName.equals("OpenAIRE Guidelines for Literature Repositories Profile v3")) {
            return new LiteratureGuidelinesV3Profile();

        } else if (guidelinesProfileName.equals("OpenAIRE Guidelines for Literature Repositories Profile v4")) {
            return new LiteratureGuidelinesV4Profile();

        } else if (guidelinesProfileName.equals("OpenAIRE FAIR Guidelines for Data Repositories Profile")) {//in case they give only fair. TODO: is ti possible?
            return new FAIR_Data_GuidelinesProfile();
        }

        return null;
    }

    public static AbstractOpenAireProfile initializeFairProfile(String guidelinesProfileName) {
        if (guidelinesProfileName.equals("OpenAIRE Guidelines for Data Archives Profile v2")) {
            return new FAIR_Data_GuidelinesProfile();

        } else if (guidelinesProfileName.equals("OpenAIRE Guidelines for Literature Repositories Profile v4")) {
            return new FAIR_Literature_GuidelinesV4Profile();
        }

        return null;
    }
}
