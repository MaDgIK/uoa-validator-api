package eu.dnetlib.validatorapi.utils;

public class PrefixInitializer {

    public static String initializeMetadataPrefix(String guidelinesProfileName) {

        if (guidelinesProfileName.equals("OpenAIRE Guidelines for Data Archives Profile v2") ||
                guidelinesProfileName.equals("OpenAIRE FAIR Guidelines for Data Repositories Profile")) {
            return "oai_datacite";

        } else if (guidelinesProfileName.equals("OpenAIRE Guidelines for Literature Repositories Profile v3")) {
            return "oai_dc";

        } else if (guidelinesProfileName.equals("OpenAIRE Guidelines for Literature Repositories Profile v4")) {
            return "oai_openaire";
        }

        return null;
    }

}
