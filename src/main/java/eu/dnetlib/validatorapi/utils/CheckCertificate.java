package eu.dnetlib.validatorapi.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.net.URL;
import java.security.cert.Certificate;

public class CheckCertificate {
    private static final Logger log = LogManager.getLogger(CheckCertificate.class);

    public static boolean isValidCertificate(String httpsURL) {
        try {
            URL myUrl = new URL(httpsURL);
            HttpsURLConnection conn = (HttpsURLConnection) myUrl.openConnection();
            conn.connect();

            // Get the certificate
            Certificate[] certs = conn.getServerCertificates();
            conn.disconnect();

            return true;

        } catch (SSLPeerUnverifiedException ssle) {
            log.error("Certificate Validation Error.", ssle);
            return false;

        } catch (Exception e) {
            log.error("Certificate Validation Error.", e);
            return false;
        }
    }
}

