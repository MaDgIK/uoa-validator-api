<?xml version="1.0" encoding="UTF-8" ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
    <responseDate>2020-04-06T13:57:50Z</responseDate>
    <request verb="ListRecords" metadataPrefix="oai_dc" set="ec_fundedresources">https://dias.library.tuc.gr/oaiHandler
    </request>
    <ListRecords>
        <record>
            <header>
                <identifier>oai:dlib.tuc.gr:21811_oa</identifier>
                <datestamp>2012-10-10T00:00:00Z</datestamp>
            </header>
            <metadata>
                <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/"
                           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/   http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                    <dc:type>info:eu-repo/semantics/article</dc:type>
                    <dc:title xml:lang="en">Mainstream traffic flow control on freeways using variable speed limits</dc:title>
                    <dc:creator xml:lang="en">Carlson Rodrigo Castelan ()</dc:creator>
                    <dc:creator xml:lang="el">Παπαμιχαηλ Ιωαννης(http://users.isc.tuc.gr/~ipapa)</dc:creator>
                    <dc:creator xml:lang="en">Papamichail Ioannis(http://users.isc.tuc.gr/~ipapa)</dc:creator>
                    <dc:creator xml:lang="el">Παπαγεωργιου Μαρκος(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Papageorgiou Markos(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:subject xml:lang="en">Traffic incident management,traffic congestion management,traffic incident
                        management
                    </dc:subject>
                    <dc:identifier>http://purl.tuc.gr/dl/dias/CF2DBEC9-EA4E-4D15-931D-C8D5D903D718</dc:identifier>
                    <dc:identifier>10.4237/transportes.v21i3.694</dc:identifier>
                    <dc:date>Published at: 2014-09-25</dc:date>
                    <dc:language>en</dc:language>
                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>
                    <dc:rights xml:lang="en">License: http://creativecommons.org/licenses/by-nc-nd/4.0/</dc:rights>
                    <dc:format>application/pdf</dc:format>
                    <dc:date>Issued on: 2013</dc:date>
                    <dc:description xml:lang="en">Summarization: Mainstream Traffic Flow Control (MTFC), enabled via variable
                        speed limits, is a control concept for real-time freeway traffic management. The benefits of MTFC for
                        efficient freeway traffic flow have been demonstrated recently using an optimal control approach and a
                        feedback control approach. In this paper, both control approaches are reviewed and applied to a freeway
                        network in a simulation environment. The validated network model used reflects an actual freeway (a
                        ring-road), fed with actual (measured) demands. The optimal and feedback control results are discussed,
                        compared and demonstrated to improve significantly the system performance. In particular, the feedback
                        control scheme is deemed suitable for immediate practical application as it takes into account operational
                        requirements and constraints, while its results are shown to be satisfactory. In addition, the control
                        system performance was not very sensitive to variations of the parameters of the feedback controller. This
                        result indicates that the burden associated with fine tuning of the controller may be reduced in the field.
                    </dc:description>
                    <dc:publisher xml:lang="en">ANPET - Associação Nacional de Pesquisa e Ensino em Transportes</dc:publisher>
                    <dc:description xml:lang="en">Presented on: Transportes</dc:description>
                    <dc:type>peer-reviewed</dc:type>
                    <dc:identifier xml:lang="en">Bibliographic citation: R.C. Carlson, I. Papamichail, M. Papageorgiou,
                        "Mainstream traffic flow control on freeways using variable speed limits," Transportes, vol. 21, no. 3, pp.
                        56-65, 2013.
                        doi:10.4237/transportes.v21i3.694
                    </dc:identifier>
                    <dc:relation>info:eu-repo/grantAgreement/EC/FP7/246686</dc:relation>
                </oai_dc:dc>
            </metadata>
        </record>
        <record>
            <header>
                <identifier>oai:dlib.tuc.gr:22973_oa</identifier>
                <datestamp>2012-10-10T00:00:00Z</datestamp>
            </header>
            <metadata>
                <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/"
                           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/   http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                    <dc:type>info:eu-repo/semantics/conferenceObject</dc:type>
                    <dc:title xml:lang="en">Motorway flow optimization in presence of vehicle automation and communication
                        systems
                    </dc:title>
                    <dc:creator xml:lang="en">Roncoli Claudio()</dc:creator>
                    <dc:creator xml:lang="el">Παπαμιχαηλ Ιωαννης(http://users.isc.tuc.gr/~ipapa)</dc:creator>
                    <dc:creator xml:lang="en">Papamichail Ioannis(http://users.isc.tuc.gr/~ipapa)</dc:creator>
                    <dc:creator xml:lang="el">Παπαγεωργιου Μαρκος(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Papageorgiou Markos(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:description xml:lang="en">The research leading to these results has received funding from the European
                        Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement
                        n. 321132, project TRAMAN21.
                    </dc:description>
                    <dc:subject xml:lang="en">Motorway traffic control</dc:subject>
                    <dc:subject xml:lang="en">Traffic flow optimisation</dc:subject>
                    <dc:subject xml:lang="en">Quadratic programming</dc:subject>
                    <dc:identifier>http://purl.tuc.gr/dl/dias/9BEA2A38-AFF2-49E1-B344-30E2C7BD34B5</dc:identifier>
                    <dc:date>Published at: 2014-10-12</dc:date>
                    <dc:language>en</dc:language>
                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>
                    <dc:rights xml:lang="en">License: http://creativecommons.org/licenses/by-nc-nd/4.0/</dc:rights>
                    <dc:format>application/pdf</dc:format>
                    <dc:date>Issued on: 2014</dc:date>
                    <dc:description xml:lang="en">Summarization: This paper describes a novel approach for defining optimal
                        strategies in motorway traffic flow control, considering that a portion of vehicles are equipped with
                        vehicle automation and communication systems. An optimisation problem, formulated as a Quadratic Programming
                        (QP) problem, is developed with the purpose of minimising traffic congestion. The proposed problem is based
                        on a first-order macroscopic traffic flow model able to capture the lane changing and the capacity drop
                        phenomena. An application example demonstrates the achievable improvements in terms of the Total Time Spent
                        if the vehicles travelling on the motorway are influenced by the control actions computed as a solution of
                        the optimisation problem.
                    </dc:description>
                    <dc:description xml:lang="el">Παρουσιάστηκε στο: International Conference on Engineering and Applied Sciences
                        Optimization
                    </dc:description>
                    <dc:publisher xml:lang="en">National Technical University of Athens</dc:publisher>
                    <dc:identifier xml:lang="el">Βιβλιογραφική αναφορά: C. Roncoli, M. Papageorgiou, I. Papamichail, "Motorway
                        flow optimization in presence of vehicle automation and communication systems," in Proceedings of the 1st
                        International Conference on Engineering and Applied Science Optimization (OPT-i), 2014, pp. 519-529.
                    </dc:identifier>
                    <dc:type>full paper</dc:type>
                    <dc:relation>info:eu-repo/grantAgreement/EC/FP7/246686</dc:relation>
                </oai_dc:dc>
            </metadata>
        </record>
        <record>
            <header>
                <identifier>oai:dlib.tuc.gr:22969_oa</identifier>
                <datestamp>2012-10-10T00:00:00Z</datestamp>
            </header>
            <metadata>
                <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/"
                           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/   http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                    <dc:type>info:eu-repo/semantics/conferenceObject</dc:type>
                    <dc:title xml:lang="en">On microscopic modelling of adaptive cruise control systems</dc:title>
                    <dc:creator xml:lang="el">Ντουσακης Ιωαννης-Αντωνιος(http://users.isc.tuc.gr/~intousakis1)</dc:creator>
                    <dc:creator xml:lang="en">Ntousakis Ioannis-Antonios(http://users.isc.tuc.gr/~intousakis1)</dc:creator>
                    <dc:creator xml:lang="el">Νικολος Ιωαννης(http://users.isc.tuc.gr/~inikolos)</dc:creator>
                    <dc:creator xml:lang="en">Nikolos Ioannis(http://users.isc.tuc.gr/~inikolos)</dc:creator>
                    <dc:creator xml:lang="el">Παπαγεωργιου Μαρκος(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Papageorgiou Markos(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:description xml:lang="en">The research leading to these results has received funding from the European
                        Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement
                        n. 321132, project TRAMAN21.
                    </dc:description>
                    <dc:subject xml:lang="en">Adaptive cruise control</dc:subject>
                    <dc:subject xml:lang="en">Traffic flow modelling</dc:subject>
                    <dc:subject xml:lang="en">Microscopic simulation</dc:subject>
                    <dc:identifier>http://purl.tuc.gr/dl/dias/F89B7182-6D1F-4179-A6D8-A875F09FD053</dc:identifier>
                    <dc:date>Published at: 2014-10-12</dc:date>
                    <dc:language>en</dc:language>
                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>
                    <dc:rights xml:lang="en">License: http://creativecommons.org/licenses/by-nc-nd/4.0/</dc:rights>
                    <dc:format>application/pdf</dc:format>
                    <dc:date>Issued on: 2014</dc:date>
                    <dc:description xml:lang="en">Summarization: The Adaptive Cruise Control (ACC) system, is one of the emerging
                        vehicle technologies that has already been deployed in the market. Although it was designed mainly to
                        enhance driver comfort and passengers’ safety, it also affects the dynamics of traffic flow. For this
                        reason, a strong research interest in the field of modelling and simulation of ACC-equipped vehicles has
                        been increasingly observed in the last years. In this work, previous modelling efforts reported in the
                        literature are reviewed, and some critical aspects to be considered when designing or simulating such
                        systems are discussed. Moreover, the integration of ACC-equipped vehicle simulation in the commercial
                        traffic simulator Aimsun is described; this is subsequently used to run simulations for different
                        penetration rates of ACC-equipped vehicles, different desired time-gap settings and different networks, to
                        assess their impact on traffic flow characteristics.
                    </dc:description>
                    <dc:description xml:lang="el">Παρουσιάστηκε στο: International Symposium of Transport Simulation 2014
                    </dc:description>
                    <dc:publisher xml:lang="en">Elsevier</dc:publisher>
                    <dc:identifier xml:lang="en">Bibliographic citation: I.A. Ntousakis, I.K. Nikolos, M. Papageorgiou, "On
                        Microscopic Modelling Of Adaptive Cruise Control Systems," in Proceedings of the 4th International Symposium
                        of Transport Simulation (ISTS), 2014.
                    </dc:identifier>
                    <dc:type>full paper</dc:type>
                    <dc:relation>info:eu-repo/grantAgreement/EC/FP7/246686</dc:relation>
                </oai_dc:dc>
            </metadata>
        </record>
        <record>
            <header>
                <identifier>oai:dlib.tuc.gr:22968_oa</identifier>
                <datestamp>2012-10-10T00:00:00Z</datestamp>
            </header>
            <metadata>
                <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/"
                           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/   http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                    <dc:type>info:eu-repo/semantics/conferenceObject</dc:type>
                    <dc:title xml:lang="en">Stability investigation for simple PI-controlled traffic systems</dc:title>
                    <dc:creator xml:lang="en">Karafyllis Iason()</dc:creator>
                    <dc:creator xml:lang="el">Παπαγεωργιου Μαρκος(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Papageorgiou Markos(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:description xml:lang="en">The research leading to these results has received funding from the European
                        Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement
                        n. 321132, project TRAMAN21.
                    </dc:description>
                    <dc:subject xml:lang="en">Systems, Nonlinear,nonlinear systems,systems nonlinear</dc:subject>
                    <dc:subject xml:lang="en">DES (System analysis),Discrete event systems,Sampled-data systems,discrete time
                        systems,des system analysis,discrete event systems,sampled data systems
                    </dc:subject>
                    <dc:subject xml:lang="en">PI-regulator</dc:subject>
                    <dc:identifier>http://purl.tuc.gr/dl/dias/A5DB2E56-C2C1-4AF6-A769-6BE271182E1C</dc:identifier>
                    <dc:identifier>978-1-4799-2889-7 10.1109/ISCCSP.2014.6877846</dc:identifier>
                    <dc:date>Published at: 2014-10-13</dc:date>
                    <dc:language>en</dc:language>
                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>
                    <dc:rights xml:lang="en">License: http://creativecommons.org/licenses/by-nc-nd/4.0/</dc:rights>
                    <dc:format>application/pdf</dc:format>
                    <dc:date>Issued on: 2014</dc:date>
                    <dc:description xml:lang="en">Summarization: This paper provides sufficient conditions for the Input-to-State
                        Stability property of simple uncertain vehicular-traffic network models under the effect of a PI-regulator.
                        Local stability properties for vehicular-traffic networks under the effect of PI-regulator control are
                        studied as well: the region of attraction of a locally exponentially stable equilibrium point is estimated
                        by means of Lyapunov functions.
                    </dc:description>
                    <dc:description xml:lang="el">Παρουσιάστηκε στο: 6th International Symposium on Communications, Controls, and
                        Signal Processing
                    </dc:description>
                    <dc:publisher xml:lang="en">Institute of Electrical and Electronics Engineers</dc:publisher>
                    <dc:identifier xml:lang="en">Bibliographic citation: I. Karafyllis, M. Papageorgiou, "Stability investigation
                        for simple PI-controlled traffic systems," in Proceedings of 6th International Symposium on Communications,
                        Controls, and Signal Processing, 2014, pp. 186-189.
                    </dc:identifier>
                    <dc:type>full paper</dc:type>
                    <dc:relation>info:eu-repo/grantAgreement/EC/FP7/246686</dc:relation>
                </oai_dc:dc>
            </metadata>
        </record>
        <record>
            <header>
                <identifier>oai:dlib.tuc.gr:24473_oa</identifier>
                <datestamp>2012-10-10T00:00:00Z</datestamp>
            </header>
            <metadata>
                <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dc="http://purl.org/dc/elements/1.1/"
                           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/   http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                    <dc:type>info:eu-repo/semantics/article</dc:type>
                    <dc:title xml:lang="en">Microsimulation analysis of practical aspects of traffic control with variable speed
                        limits
                    </dc:title>
                    <dc:creator xml:lang="el">Παπαγεωργιου Μαρκος(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Papageorgiou Markos(http://users.isc.tuc.gr/~mpapageorgiou)</dc:creator>
                    <dc:creator xml:lang="en">Muller Eduardo Rauh()</dc:creator>
                    <dc:creator xml:lang="en">Carlson Rodrigo Castelan()</dc:creator>
                    <dc:creator xml:lang="en">Kraus Werner()</dc:creator>
                    <dc:description xml:lang="en">The research leading to these results has received funding from the European
                        Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement
                        n. 321132, project TRAMAN21. The paper has been published by IEEE
                        (http://ieeexplore.ieee.org/Xplore/defdeny.jsp?url=http%3A%2F%2Fieeexplore.ieee.org%2Fstamp%2Fstamp.jsp%3Ftp%3D%26arnumber%3D7006741&amp;denyReason=-134&amp;arnumber=7006741&amp;productsMatched=null)
                        and is IEEE copyrighted.
                    </dc:description>
                    <dc:subject xml:lang="en">Traffic volume,traffic flow,traffic volume</dc:subject>
                    <dc:subject xml:lang="en">Mainstream Traffic Flow Control</dc:subject>
                    <dc:subject xml:lang="en">Variable Speed Limits</dc:subject>
                    <dc:subject xml:lang="en">Freeway traffic control</dc:subject>
                    <dc:identifier>http://purl.tuc.gr/dl/dias/1968E8C7-94C6-4EB3-A513-C686F3F12B79</dc:identifier>
                    <dc:identifier>10.1109/TITS.2014.2374167</dc:identifier>
                    <dc:date>Published at: 2015-03-26</dc:date>
                    <dc:language>en</dc:language>
                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>
                    <dc:rights xml:lang="en">License: http://creativecommons.org/licenses/by-nc-nd/4.0/</dc:rights>
                    <dc:format>application/pdf</dc:format>
                    <dc:date>Issued on: 2015</dc:date>
                    <dc:description xml:lang="en">Summarization: Mainstream traffic flow control (MTFC) with variable speed limits
                        (VSLs) is a freeway traffic control method that aims to maximize throughput by regulating the mainstream
                        flow upstream from a bottleneck. Previous studies in a macroscopic simulator have shown optimal and feedback
                        MTFC potential to improve traffic conditions. In this paper, local feedback MTFC is applied in microscopic
                        simulation for an on-ramp merge bottleneck. Traffic behavior reveals important aspects that had not been
                        previously captured in macroscopic simulation. Mainly, the more realistic VSL application at specific points
                        instead of along an entire freeway section produces a slower traffic response to speed limit changes. In
                        addition, the nonlinear capacity flow/speed limit relation observed in the microscopic model is more
                        pronounced than what was observed at the macroscopic level. After appropriate modifications in the control
                        law, significant improvements in traffic conditions are obtained.
                    </dc:description>
                    <dc:publisher xml:lang="en">Institute of Electrical and Electronics Engineers</dc:publisher>
                    <dc:description xml:lang="en">Presented on: IEEE Transactions on Intelligent Transportation Systems
                    </dc:description>
                    <dc:type>peer-reviewed</dc:type>
                    <dc:identifier xml:lang="en">Bibliographic citation: E.R. Müller, R.C. Carlson, W. Kraus Jr, M. Papageorgiou,
                        "Microsimulation analysis of practical aspects of traffic control with variable speed limits," IEEE
                        Transactions on Intelligent Transportation Systems, vol. 16, no.1, pp. 512-523, 2015.
                    </dc:identifier>
                    <dc:relation>info:eu-repo/grantAgreement/EC/FP7/246686</dc:relation>
                </oai_dc:dc>
            </metadata>
        </record>

        <resumptionToken expirationDate="2020-04-07T16:59:31Z" completeListSize="14459" cursor="0">1586181471310:302
        </resumptionToken>
    </ListRecords>
</OAI-PMH>
